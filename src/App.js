import logo from './logo.svg';
import './App.css';
import NavBarCustom from './components/NavBarCustom';
import HeroCustom from './components/HeroCustom';
import MovieCard from './components/MovieCard';
import SearchFilter from './components/SearchFilter';
import AddMovie from './components/AddMovie';
import { MovieList } from './components/MovieList';

function App() {
  return (
    <div className="App">
      <NavBarCustom />
      <HeroCustom />
      <MovieList/>
      <SearchFilter />
      <AddMovie/>
    </div>
  );
}

export default App;
